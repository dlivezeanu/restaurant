package com.endava.levelup2020.restaurant.controller;

import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.model.NewOrder;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.model.Product;
import com.endava.levelup2020.restaurant.model.Restaurant;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.service.OrderService;
import com.endava.levelup2020.restaurant.service.ProductService;
import com.endava.levelup2020.restaurant.service.RestaurantService;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestaurantController implements RestaurantApi {

  private final RestaurantService restaurantService;
  private final ProductService productService;
  private final OrderService orderService;
  private final Mapper mapper;

  @Autowired
  public RestaurantController(
      RestaurantService restaurantService,
      ProductService productService,
      OrderService orderService, Mapper mapper) {
    this.restaurantService = restaurantService;
    this.productService = productService;
    this.orderService = orderService;
    this.mapper = mapper;

  }

  @Override
  public ResponseEntity<List<Product>> getProductsForRestaurant(UUID id, @Valid String name,
                                                                @Valid String category) {
    ServiceInvocationOutcome<List<ProductEntity>> outcome =
        productService.getRestaurantProductsByNameAndByCategory(id, name, category);
    if (outcome.notFound) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(outcome.response
        .stream()
        .map(product -> mapper.map(product, Product.class))
        .collect(Collectors.toList()), HttpStatus.OK);
  }


  @Override
  public ResponseEntity<List<Restaurant>> getRestaurants(
      @Valid String name, @Valid String location) {
    return ResponseEntity.ok(restaurantService.findByNameAndArea(name, location)
        .stream()
        .map(restaurant -> mapper.map(restaurant, Restaurant.class))
        .collect(Collectors.toList()));
  }

  @Override
  public ResponseEntity<OrderDetails> openOrder(UUID id,
                                                @Valid NewOrder newOrder) {

    ServiceInvocationOutcome<OrderDetails> response = orderService.openOrder(id, newOrder);
    if (response.notFound) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (response.badRequest) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(response.response, HttpStatus.OK);

  }

  @Override
  public ResponseEntity<Void> orderReady(UUID id) {
    ServiceInvocationOutcome<OrderEntity> response = orderService.orderReady(id);
    if (response.notFound) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (response.badRequest) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(HttpStatus.OK);

  }

  @Override
  public ResponseEntity<OrderDetails> getOrderDetails(UUID idOrder) {

    return orderService.getOrderById(idOrder).map(orderEntity -> new ResponseEntity<>(
        mapper.map(orderService.getOrderDetails(orderEntity).get(), OrderDetails.class),
        HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

}
