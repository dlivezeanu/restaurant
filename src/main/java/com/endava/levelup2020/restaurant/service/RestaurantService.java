package com.endava.levelup2020.restaurant.service;

import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface RestaurantService {


  List<RestaurantEntity> findByNameAndArea(String name, String location);

  Optional<RestaurantEntity> findById(UUID id);

}
