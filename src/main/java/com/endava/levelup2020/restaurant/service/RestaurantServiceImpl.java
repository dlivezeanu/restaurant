package com.endava.levelup2020.restaurant.service;


import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RestaurantServiceImpl implements RestaurantService {

  private RestaurantRepo restaurantRepo;

  @Autowired
  public RestaurantServiceImpl(
      RestaurantRepo restaurantRepo) {
    this.restaurantRepo = restaurantRepo;
  }

  @Override
  public Optional<RestaurantEntity> findById(UUID id) {
    return restaurantRepo.findById(id);
  }

  @Override
  public List<RestaurantEntity> findByNameAndArea(String name, String location) {

    return restaurantRepo.findByNameAndArea(name, location);
  }


}
