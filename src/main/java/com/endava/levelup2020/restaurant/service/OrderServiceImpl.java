package com.endava.levelup2020.restaurant.service;

import com.endava.levelup2020.restaurant.entity.CustomerEntity;
import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.integration.delivery.payload.request.OrderPending;
import com.endava.levelup2020.restaurant.integration.feedback.payload.request.OrderRegistration;
import com.endava.levelup2020.restaurant.model.NewOrder;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.payload.request.NewOrderBody;
import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.repository.OrderRepo;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderServiceImpl implements OrderService {

  private final  OrderRepo orderRepo;
  private final RestaurantRepo restaurantRepo;
  private final RestTemplate restTemplate;
  private final ProductService productService;

  @Autowired
  public OrderServiceImpl(OrderRepo orderRepo,RestaurantRepo restaurantRepo,
                          RestTemplate restTemplate,
                          ProductService productService) {
    this.orderRepo = orderRepo;
    this.restaurantRepo=restaurantRepo;
    this.restTemplate = restTemplate;
    this.productService=productService;
  }

  @Override
  public Optional<OrderEntity> getOrderById(UUID id) {
    return orderRepo.findById(id);
  }

  @Override
  public void storeOrder(OrderEntity orderEntity) {
    orderRepo.save(orderEntity);
  }

  @Override
  public Optional<OrderDetail> getOrderDetails(OrderEntity orderEntity) {
    List<DetailedOrderedProductEntity> orderedProductEntityList =
        productService.getProductsFromIdList(orderEntity.getRestaurantId(), orderEntity.getProducts());
    if (orderedProductEntityList.isEmpty())
      return Optional.empty();
    OrderDetail orderDetails=new OrderDetail();
    orderDetails.setId(orderEntity.getId());
    orderDetails.setProducts(orderedProductEntityList);
    orderDetails.setComments(orderEntity.getComments());
    orderDetails.setStatus(orderEntity.getStatus());
    orderDetails.setCustomer(orderEntity.getCustomer());
    orderDetails.setRegistrationDate(orderEntity.getRegistrationDate());
    orderDetails.setPrice(orderDetails.calculateTotalPrice());
    orderDetails.setRestaurantId(orderEntity.getRestaurantId());
    return Optional.of(orderDetails);

  }

  @Override
  public ServiceInvocationOutcome openOrder(UUID restaurantId, NewOrder newOrderPayload)
  {
    Mapper mapper = DozerBeanMapperBuilder.create().build();
    NewOrderBody newOrder=mapper.map(newOrderPayload,NewOrderBody.class);

    Optional<RestaurantEntity> restaurant=restaurantRepo.findById(restaurantId);
    if (!restaurant.isPresent())
      return ServiceInvocationOutcome.NOT_FOUND("Restaurant "+restaurantId+" not found");

    OrderEntity orderEntity = mapper.map(newOrder, OrderEntity.class);
    orderEntity.setRestaurantId(restaurant.get().getId());
    orderEntity.setCustomer(mapper.map(newOrder.getCustomer(), CustomerEntity.class));
    Optional<OrderDetail> orderDetails = getOrderDetails(orderEntity);
    if (!orderDetails.isPresent())
      return ServiceInvocationOutcome.BAD_REQUEST("Product not found");
    orderEntity.setPrice(orderDetails.get().getPrice());
    storeOrder(orderEntity);

    String url = "http://localhost:8081/delivery/order/pending";
    OrderPending orderPending = new OrderPending(orderDetails.get(), restaurant.get());
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<OrderPending> orderPendingHttpEntity = new HttpEntity<>(orderPending, httpHeaders);
    ResponseEntity<String> response = restTemplate.postForEntity(url, orderPendingHttpEntity, String.class);
    if (response.getStatusCode()==HttpStatus.NOT_FOUND)
      return ServiceInvocationOutcome.NOT_FOUND(response.getBody());


    url = "http://localhost:8082/feedback/order";
    OrderRegistration orderRegistration =
        new OrderRegistration(orderDetails.get(), restaurant.get());
    HttpEntity<OrderRegistration> orderRegistrationHttpEntity =
        new HttpEntity<>(orderRegistration, httpHeaders);
    response=restTemplate.postForEntity(url, orderRegistrationHttpEntity, String.class);
    if (response.getStatusCode()==HttpStatus.NOT_FOUND)
      return ServiceInvocationOutcome.NOT_FOUND(response.getBody());

    return ServiceInvocationOutcome.OK(mapper.map(orderDetails.get(),OrderDetails.class));
  }

  @Override
  public ServiceInvocationOutcome orderReady(UUID id) {
    Optional<OrderEntity> order = getOrderById(id);
    if (order.isPresent())
    {
      if (order.get().getStatus() != OrderDetails.StatusEnum.CREATED)
        return ServiceInvocationOutcome.BAD_REQUEST("Already completed");

        order.get().setStatus(OrderDetails.StatusEnum.READY);
        String url = "http://localhost:8081/delivery/order/ready/" + order.get().getId();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response =
            restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
        if (response.getStatusCode() == HttpStatus.NOT_FOUND)
          return ServiceInvocationOutcome.NOT_FOUND(response.getBody());
        storeOrder(order.get());
        return ServiceInvocationOutcome.OK(order.get());

    }
    return ServiceInvocationOutcome.NOT_FOUND("Order "+id+" not found");
  }
}
