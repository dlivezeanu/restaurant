package com.endava.levelup2020.restaurant.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;


import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

@Service

public class ProductServiceImpl implements ProductService {

  private MongoTemplate mongoTemplate;
  private RestaurantRepo restaurantRepo;
  private static final String PRODUCT_ID = "products.id";
  private static final String PRODUCT_NAME = "products.name";
  private static final String PRODUCT_CATEGORY = "products.category";
  private static final String PRODUCT_PRICE = "products.price";
  private static final String PRODUCT_PREPARINGTINE = "products.preparingTime";
  private static final String PRODUCT_DESCRIPTION = "products.description";

  private static final ProjectionOperation projectToMatchModel = project()
      .andExpression(PRODUCT_ID).as("_id")
      .andExpression(PRODUCT_NAME).as("name")
      .andExpression(PRODUCT_PREPARINGTINE).as("preparingTime")
      .andExpression(PRODUCT_CATEGORY).as("category")
      .andExpression(PRODUCT_DESCRIPTION).as("description")
      .andExpression(PRODUCT_PRICE).as("price");

  @Autowired
  public ProductServiceImpl(MongoTemplate mongoTemplate,
                            RestaurantRepo restaurantRepo) {
    this.mongoTemplate = mongoTemplate;
    this.restaurantRepo = restaurantRepo;
  }

  @Override
  public ServiceInvocationOutcome getRestaurantProductsByNameAndByCategory(UUID restaurantId,
                                                                           String name,
                                                                           String categoryName) {
    if (!restaurantRepo.findById(restaurantId).isPresent()) {
      return ServiceInvocationOutcome.NOT_FOUND("Restaurant " + restaurantId + " not found");
    }
    Criteria criteria = new Criteria();
    if (name != null) {
      criteria.and(PRODUCT_NAME).is(name);
    }
    if (categoryName != null) {
      criteria.and(PRODUCT_CATEGORY).is(categoryName);
    }

    Aggregation aggregation = newAggregation(
        match(Criteria.where("id").is(restaurantId)
        ), unwind("products"), match(criteria), projectToMatchModel);
    AggregationResults<ProductEntity> list = mongoTemplate.aggregate(aggregation,
        RestaurantEntity.class, ProductEntity.class);
    return ServiceInvocationOutcome.OK(list.getMappedResults());
  }

  @Override
  public List<DetailedOrderedProductEntity> getProductsFromIdList(UUID restaurantId,
                                                                  List<OrderedProduct> products) {
    List<DetailedOrderedProductEntity> productEntities = new ArrayList<>();
    for (OrderedProduct postOrderedProduct : products) {
      Optional<ProductEntity> product =
          getProductByRestaurantIdAndProductId(restaurantId, postOrderedProduct.getProductId());
      if (product.isPresent()) {
        Mapper mapper = DozerBeanMapperBuilder.create().build();
        DetailedOrderedProductEntity orderedProductEntity =
            mapper.map(product.get(), DetailedOrderedProductEntity.class);
        orderedProductEntity.setQuantity(postOrderedProduct.getNoOfItems());
        productEntities.add(orderedProductEntity);
      } else {
        return new ArrayList<>();
      }
    }
    return productEntities;
  }

  private Optional<ProductEntity> getProductByRestaurantIdAndProductId(UUID restaurantId,
                                                                       UUID productId) {
    Aggregation aggregation = newAggregation(
        match(Criteria.where("id").is(restaurantId)
        ), unwind("products"), match(Criteria.where("products._id").is(productId)),
        projectToMatchModel);
    AggregationResults<ProductEntity> list = mongoTemplate.aggregate(aggregation,
        RestaurantEntity.class, ProductEntity.class);
    return Optional.ofNullable(list.getUniqueMappedResult());
  }
}
