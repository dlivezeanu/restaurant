package com.endava.levelup2020.restaurant.service;

import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import java.util.List;
import java.util.UUID;

public interface ProductService {

  ServiceInvocationOutcome<List<ProductEntity>> getRestaurantProductsByNameAndByCategory(UUID restaurantId, String name,
                                                                                        String categoryName);

  List<DetailedOrderedProductEntity> getProductsFromIdList(UUID restaurantId, List<OrderedProduct>products);


}
