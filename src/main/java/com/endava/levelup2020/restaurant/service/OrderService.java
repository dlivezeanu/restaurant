package com.endava.levelup2020.restaurant.service;

import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.model.NewOrder;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {
  Optional<OrderEntity> getOrderById(UUID id);

  void storeOrder(OrderEntity orderEntity);

  Optional<OrderDetail> getOrderDetails(OrderEntity orderEntity);

  ServiceInvocationOutcome<OrderEntity> orderReady(UUID id);

  ServiceInvocationOutcome<OrderDetails> openOrder(UUID restaurantId, NewOrder newOrder);
}
