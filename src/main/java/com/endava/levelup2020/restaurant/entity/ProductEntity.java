package com.endava.levelup2020.restaurant.entity;

import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;

public class ProductEntity {
  @Id
  private UUID id;
  @TextIndexed private String name;
  @TextIndexed private String category;
  private Integer preparingTime;
  private String description;
  private Double price;

  public ProductEntity() {
    setId(UUID.randomUUID());
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Integer getPreparingTime() {
    return preparingTime;
  }

  public void setPreparingTime(Integer preparingTime) {
    this.preparingTime = preparingTime;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }
}
