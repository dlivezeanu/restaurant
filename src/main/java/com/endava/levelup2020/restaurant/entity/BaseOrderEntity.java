package com.endava.levelup2020.restaurant.entity;

import com.endava.levelup2020.restaurant.model.OrderDetails;
import java.util.Date;
import java.util.UUID;

public class BaseOrderEntity {
  private UUID id;
  private UUID restaurantId;
  private OrderDetails.StatusEnum status;
  private CustomerEntity customer;
  private Date registrationDate;
  private double price;
  private String comments;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(UUID restaurantId) {
    this.restaurantId = restaurantId;
  }

  public OrderDetails.StatusEnum getStatus() {
    return status;
  }

  public void setStatus(OrderDetails.StatusEnum status) {
    this.status = status;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  public Date getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public BaseOrderEntity() {
    //
  }
}
