package com.endava.levelup2020.restaurant.entity;

import javax.validation.constraints.Positive;

public class DetailedOrderedProductEntity {

  private String category;
  private String name;
  private Integer preparingTime;
  @Positive
  private Integer quantity;
  private Double price;

  public DetailedOrderedProductEntity() {
    //
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Integer getPreparingTime() {
    return preparingTime;
  }

  public void setPreparingTime(Integer preparingTime) {
    this.preparingTime = preparingTime;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public Double getPrice() {
    return price;
  }
}
