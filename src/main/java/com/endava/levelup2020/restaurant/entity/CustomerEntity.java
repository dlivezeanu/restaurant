package com.endava.levelup2020.restaurant.entity;

import java.util.UUID;
import org.springframework.data.annotation.Id;

public class CustomerEntity {
  @Id
  private UUID id;
  private String name;

  private String address;
  private String phoneNumber;

  public CustomerEntity() {
    setId(UUID.randomUUID());
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}
