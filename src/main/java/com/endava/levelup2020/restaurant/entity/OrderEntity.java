package com.endava.levelup2020.restaurant.entity;


import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Orders")
public class OrderEntity extends BaseOrderEntity {

  private List<OrderedProduct> products = new ArrayList<>();

  public OrderEntity() {
    setId(UUID.randomUUID());
    setStatus(OrderDetails.StatusEnum.CREATED);
    setRegistrationDate(new Date());
  }


  public List<OrderedProduct> getProducts() {
    return products;
  }

  public void setProducts(
      List<OrderedProduct> products) {
    this.products = products;
  }
}
