package com.endava.levelup2020.restaurant.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Restaurants")
public class RestaurantEntity {
  @Id
  private UUID id;
  private String name;
  private String area;
  private String address;
  private List<ProductEntity> products = new ArrayList<>();

  public RestaurantEntity() {
    setId(UUID.randomUUID());
  }

  public RestaurantEntity(UUID id,
                          @NotNull @NotBlank String name,
                          @NotNull @NotBlank String area, String address,
                          List<ProductEntity> products) {
    this.id = id;
    this.name = name;
    this.area = area;
    this.address = address;
    this.products = products;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<ProductEntity> getProducts() {
    return products;
  }

  public void setProducts(List<ProductEntity> productEntities) {
    this.products = productEntities;
  }
}
