package com.endava.levelup2020.restaurant.repository;

import com.endava.levelup2020.restaurant.entity.OrderEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo extends MongoRepository<OrderEntity, UUID> {
  Optional<OrderEntity> findById(UUID id);

}
