package com.endava.levelup2020.restaurant.repository;

import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepo extends MongoRepository<RestaurantEntity, UUID> {

  Optional<RestaurantEntity> findById(UUID id);

  @Query("{$and :["
      + "?#{ [0] == null ? { $where : 'true'} : { 'name' : [0] } },"
      + "?#{ [1] == null ? { $where : 'true'} : { 'area' : [1] } }"
      + "]}")
  List<RestaurantEntity> findByNameAndArea(String name, String location);

}
