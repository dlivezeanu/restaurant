package com.endava.levelup2020.restaurant.payload.response;
@SuppressWarnings("unchecked")
public class ServiceInvocationOutcome<T> {

  public final boolean ok;
  public final boolean notFound;
  public final boolean badRequest;
  public final String errorMessage;
  public final T response;

  public ServiceInvocationOutcome(boolean ok, boolean notFound, boolean badRequest, String errorMessage, T response) {
    this.ok = ok;
    this.notFound = notFound;
    this.badRequest = badRequest;
    this.errorMessage = errorMessage;
    this.response = response;
  }
  private ServiceInvocationOutcome(T response) {
    this(true, false, false,"", response);
  }

  private ServiceInvocationOutcome(boolean notFound, boolean badRequest, String errorMessage) {
    this(false, notFound, badRequest, errorMessage,null);
  }

  public static <T> ServiceInvocationOutcome OK(T response) {
    return new ServiceInvocationOutcome(response);
  }

  public static ServiceInvocationOutcome NOT_FOUND (String errorMessage) {
    return new ServiceInvocationOutcome(true, false, errorMessage);
  }

  public static ServiceInvocationOutcome BAD_REQUEST(String errorMessage) {
    return new ServiceInvocationOutcome(false, true, errorMessage);
  }

}