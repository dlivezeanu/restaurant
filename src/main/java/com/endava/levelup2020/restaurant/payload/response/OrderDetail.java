package com.endava.levelup2020.restaurant.payload.response;

import com.endava.levelup2020.restaurant.entity.BaseOrderEntity;
import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;


public class OrderDetail extends BaseOrderEntity {

  private List<DetailedOrderedProductEntity> products = new ArrayList<>();

  public OrderDetail() {
  }

  public Double calculateTotalPrice() {
    Double total = 0.0;
    for (DetailedOrderedProductEntity productEntity : getProducts()) {
      total += productEntity.getPrice() * productEntity.getQuantity();
    }
    return total + 1;
  }

  public Date calculateTimeOfCooking() {
    Integer minutes = 0;
    for (DetailedOrderedProductEntity orderedProductEntity : getProducts()) {
      minutes += orderedProductEntity.getPreparingTime()*orderedProductEntity.getQuantity();
    }
    return DateUtils.addMinutes(super.getRegistrationDate(), minutes);

  }

  public List<DetailedOrderedProductEntity> getProducts() {
    return products;
  }

  public void setProducts(
      List<DetailedOrderedProductEntity> products) {
    this.products = products;
  }


}
