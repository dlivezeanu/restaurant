package com.endava.levelup2020.restaurant.payload.request;


import com.endava.levelup2020.restaurant.model.Customer;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import java.util.List;

public class NewOrderBody {

  private Customer customer = null;
  private List<OrderedProduct> products = null;
  private String comments = null;


  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public List<OrderedProduct> getProducts() {
    return products;
  }

  public void setProducts(List<OrderedProduct> products) {
    this.products = products;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }


}