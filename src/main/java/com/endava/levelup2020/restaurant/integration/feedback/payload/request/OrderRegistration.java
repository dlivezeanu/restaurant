package com.endava.levelup2020.restaurant.integration.feedback.payload.request;

import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import java.util.UUID;

public class OrderRegistration {

  private UUID orderId;
  private UUID restaurantId;
  private String restaurantName;
  private UUID customerId;
  private String customerName;
  private UUID deliveryManId;

  public OrderRegistration() {

  }


  public OrderRegistration(OrderDetail order, RestaurantEntity restaurant) {
    this.orderId = order.getId();
    this.restaurantId = restaurant.getId();
    this.restaurantName = restaurant.getName();
    this.customerId = order.getCustomer().getId();
    this.customerName = order.getCustomer().getName();
  }

  public UUID getDeliveryManId() {
    return deliveryManId;
  }

  public void setDeliveryManId(UUID deliveryManId) {
    this.deliveryManId = deliveryManId;
  }

  public UUID getOrderId() {
    return orderId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }

  public UUID getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(UUID restaurantId) {
    this.restaurantId = restaurantId;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public UUID getCustomerId() {
    return customerId;
  }

  public void setCustomerId(UUID customerId) {
    this.customerId = customerId;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }


}
