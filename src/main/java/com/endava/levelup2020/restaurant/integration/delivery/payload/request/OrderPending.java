package com.endava.levelup2020.restaurant.integration.delivery.payload.request;

import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import java.time.Instant;
import java.util.UUID;

public class OrderPending {

  private UUID id;
  private String status;
  private String restaurantName;
  private String restaurantAddress;
  private String restaurantArea;
  private String deliveryAddress;
  private Instant estimatedTimeOfCooking;
  public OrderPending(){}
  public OrderPending(OrderDetail order, RestaurantEntity restaurant)
  {
    this.id=order.getId();
    this.deliveryAddress=order.getCustomer().getAddress();
    this.restaurantAddress=restaurant.getAddress();
    this.restaurantArea=restaurant.getArea();
    this.restaurantName=restaurant.getName();
    this.estimatedTimeOfCooking=order.calculateTimeOfCooking().toInstant();

  }
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public String getRestaurantAddress() {
    return restaurantAddress;
  }

  public void setRestaurantAddress(String restaurantAddress) {
    this.restaurantAddress = restaurantAddress;
  }

  public String getRestaurantArea() {
    return restaurantArea;
  }

  public void setRestaurantArea(String restaurantZone) {
    this.restaurantArea = restaurantZone;
  }

  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  public Instant getEstimatedTimeOfCooking() {
    return estimatedTimeOfCooking;
  }

  public void setEstimatedTimeOfCooking(Instant estimatedTimeOfCooking) {
    this.estimatedTimeOfCooking = estimatedTimeOfCooking;
  }
}
