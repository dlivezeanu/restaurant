package com.endava.levelup2020.restaurant.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import com.endava.levelup2020.restaurant.utils.Builder;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RestaurantServiceImplTest {
  private RestaurantEntity restaurant;
  @Mock
  private RestaurantRepo restaurantRepo;
  @InjectMocks
  private RestaurantServiceImpl restaurantService;

  @BeforeEach
  void init() {
    restaurantService = new RestaurantServiceImpl(restaurantRepo);
    restaurant = Builder.restaurantBuilder();

  }

  @Test
  void findById_notNull() {

    //given
    UUID restID = Builder.RESTAURANT_ID;
    //when
    when(restaurantService.findById(restID)).thenReturn(Optional.of(restaurant));
    Optional<RestaurantEntity> restaurantEntity = restaurantService.findById(restID);
    //then
    assertNotNull(restaurantEntity.get());
  }

  @Test
  void findById_Null_reject() {

    //given
    UUID restID = Builder.RESTAURANT_ID;
    //when
    when(restaurantService.findById(restID)).thenReturn(Optional.empty());
    Optional<RestaurantEntity> restaurantEntity = restaurantService.findById(restID);
    //then
    assertFalse(restaurantEntity.isPresent());
  }


  @Test
  void findByNameAndArea_filterAllParams_accept() {

    //given
    String restName = "Rest1";
    String restArea = "Central";
    //when
    when(restaurantService.findByNameAndArea(restName, restArea))
        .thenReturn(Arrays.asList(restaurant));
    List<RestaurantEntity> restaurantList = restaurantService.findByNameAndArea(restName, restArea);
    //then
    assertFalse(restaurantList.isEmpty());
  }

  @Test
  void findByNameAndArea_nullParams_accept() {

    //given
    String restName = null;
    String restArea = null;
    //when
    when(restaurantService.findByNameAndArea(restName, restArea))
        .thenReturn(Arrays.asList(restaurant));
    List<RestaurantEntity> restaurantList = restaurantService.findByNameAndArea(restName, restArea);
    //then
    assertFalse(restaurantList.isEmpty());
  }

}