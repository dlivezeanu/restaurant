package com.endava.levelup2020.restaurant.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.repository.OrderRepo;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import com.endava.levelup2020.restaurant.utils.Builder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
  private OrderEntity order;
  private UUID restId;

  @Mock
  private OrderRepo orderRepo;

  @Mock
  private RestaurantRepo restaurantRepo;

  @Mock
  private RestTemplate restTemplate;

  @Mock
  private ProductServiceImpl productService;

  @InjectMocks
  private OrderServiceImpl orderService;


  @BeforeEach
  void init() {
    orderService = new OrderServiceImpl(orderRepo, restaurantRepo, restTemplate, productService);
    order = Builder.orderBuilder();
    restId = Builder.RESTAURANT_ID;
  }

  @Test
  void findById_notNull() {

    //given
    UUID orderID = Builder.ORDER_ID;
    //when
    when(orderService.getOrderById(orderID)).thenReturn(Optional.of(order));
    Optional<OrderEntity> orderEntity = orderService.getOrderById(orderID);
    //then
    assertNotNull(orderEntity.get());
  }

  @Test
  void findById_Null() {

    //given
    UUID orderID = Builder.ORDER_ID;
    //when
    when(orderService.getOrderById(orderID)).thenReturn(Optional.empty());
    Optional<OrderEntity> orderEntity = orderService.getOrderById(orderID);
    //then
    assertFalse(orderEntity.isPresent());
  }

  @Test
  void getOrderDetail_accept() {

    //given
    OrderEntity orderEntity = order;
    //when
    OrderedProduct orderedProduct = Builder.orderedProductBuilder();
    DetailedOrderedProductEntity detailedProduct = Builder.detailedProductBuilder();

    when(productService.getProductsFromIdList(Builder.RESTAURANT_ID,
        Collections.singletonList(orderedProduct)))
        .thenReturn(Collections.singletonList(detailedProduct));
    Optional<OrderDetail> orderDetail = orderService.getOrderDetails(orderEntity);
    //then
    assertEquals(orderEntity.getRegistrationDate(), orderDetail.get().getRegistrationDate());
  }

  @Test
  void orderReady_accept() {
    //given
    UUID orderId = order.getId();
    //when
    when(orderService.getOrderById(orderId)).thenReturn(Optional.of(order));
    String url = "http://localhost:8081/delivery/order/ready/" + order.getId();
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(httpHeaders);
    ResponseEntity<String> responseEntity = ResponseEntity.ok("ok");
    when(restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class))
        .thenReturn(responseEntity);
    ServiceInvocationOutcome result = orderService.orderReady(orderId);
    //then
    assertTrue(result.ok);

  }

  @Test
  void orderReady_orderNotFound_reject() {
    //given
    UUID orderId = order.getId();
    //when
    when(orderService.getOrderById(orderId)).thenReturn(Optional.empty());
    ServiceInvocationOutcome result = orderService.orderReady(orderId);
    //then
    assertTrue(result.notFound);

  }

  @Test
  void orderReady_orderAlreadyCompleted_reject() {
    //given
    order.setStatus(OrderDetails.StatusEnum.READY);
    UUID orderId = order.getId();
    //when
    when(orderService.getOrderById(orderId)).thenReturn(Optional.of(order));
    ServiceInvocationOutcome result = orderService.orderReady(orderId);
    //then
    assertTrue(result.badRequest);
  }

  @Test
  void orderReady_reject_deliveryService() {
    //given
    UUID orderId = order.getId();
    //when
    when(orderService.getOrderById(orderId)).thenReturn(Optional.of(order));
    String url = "http://localhost:8081/delivery/order/ready/" + order.getId();
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(httpHeaders);
    ResponseEntity<String> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    when(restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class))
        .thenReturn(responseEntity);
    ServiceInvocationOutcome result = orderService.orderReady(orderId);
    //then
    assertTrue(result.notFound);
  }

  @Test
  void openOrder_accept() {
    //given
    UUID restId = Builder.RESTAURANT_ID;
    OrderEntity orderEntity = order;
    RestaurantEntity restaurant = Builder.restaurantBuilder();
    DetailedOrderedProductEntity detailedProduct = Builder.detailedProductBuilder();
    ResponseEntity<String> deliveryServiceResponse = ResponseEntity.ok("ok");

    //when
    when(restaurantRepo.findById(restId)).thenReturn(Optional.of(restaurant));
    when(productService.getProductsFromIdList(restaurant.getId(), orderEntity.getProducts()))
        .thenReturn(Collections.singletonList(detailedProduct));

    when(restTemplate.postForEntity(anyString(), any(), eq(String.class)))
        .thenReturn(deliveryServiceResponse);

    ServiceInvocationOutcome result = orderService.openOrder(restId, Builder.orderPayloadBuilder());
    //then
    assertTrue(result.ok);

  }

  @Test
  void openOrder_restaurantNotFound_reject() {

    //when
    when(restaurantRepo.findById(restId)).thenReturn(Optional.empty());
    ServiceInvocationOutcome result = orderService.openOrder(restId, Builder.orderPayloadBuilder());
    //then
    assertTrue(result.notFound);
  }

  @Test
  void openOrder_productNotFound_reject() {
    //given

    RestaurantEntity restaurant = Builder.restaurantBuilder();
    //when
    when(restaurantRepo.findById(restId)).thenReturn(Optional.of(restaurant));
    when(productService.getProductsFromIdList(restaurant.getId(), order.getProducts()))
        .thenReturn(new ArrayList<>());
    ServiceInvocationOutcome result = orderService.openOrder(restId, Builder.orderPayloadBuilder());
    //then
    assertTrue(result.badRequest);
  }

  @Test
  void openOrder_deliveryRequest_reject() {
    //given
    RestaurantEntity restaurant = Builder.restaurantBuilder();
    DetailedOrderedProductEntity detailedProduct = Builder.detailedProductBuilder();
    ResponseEntity<String> deliveryServiceResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    //when
    when(restaurantRepo.findById(restId)).thenReturn(Optional.of(restaurant));
    when(productService.getProductsFromIdList(restaurant.getId(), order.getProducts()))
        .thenReturn(Collections.singletonList(detailedProduct));
    when(restTemplate
        .postForEntity(eq("http://localhost:8081/delivery/order/pending"), any(), eq(String.class)))
        .thenReturn(deliveryServiceResponse);
    ServiceInvocationOutcome result = orderService.openOrder(restId, Builder.orderPayloadBuilder());
    //then
    assertTrue(result.notFound);
  }

  @Test
  void openOrder_feedbackRequest_reject() {
    //given
    RestaurantEntity restaurant = Builder.restaurantBuilder();
    DetailedOrderedProductEntity detailedProduct = Builder.detailedProductBuilder();
    ResponseEntity<String> deliveryServiceResponse = ResponseEntity.ok("ok");
    ResponseEntity<String> feedbackServiceResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    //when
    when(restaurantRepo.findById(restId)).thenReturn(Optional.of(restaurant));
    when(productService.getProductsFromIdList(restaurant.getId(), order.getProducts()))
        .thenReturn(Collections.singletonList(detailedProduct));
    when(restTemplate
        .postForEntity(eq("http://localhost:8081/delivery/order/pending"), any(), eq(String.class)))
        .thenReturn(deliveryServiceResponse);
    when(restTemplate
        .postForEntity(eq("http://localhost:8082/feedback/order"), any(), eq(String.class)))
        .thenReturn(feedbackServiceResponse);
    ServiceInvocationOutcome result = orderService.openOrder(restId, Builder.orderPayloadBuilder());
    //then
    assertTrue(result.notFound);

  }


}