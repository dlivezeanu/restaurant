package com.endava.levelup2020.restaurant.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.repository.RestaurantRepo;
import com.endava.levelup2020.restaurant.utils.Builder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {


  @InjectMocks
  private ProductServiceImpl productService;
  @Mock
  private MongoTemplate mongoTemplate;
  @Mock
  private RestaurantRepo restaurantRepo;
  RestaurantEntity restaurantEntity;

  @BeforeEach
  public void init() {
    productService = new ProductServiceImpl(mongoTemplate, restaurantRepo);
    restaurantEntity = Builder.restaurantBuilder();
  }

  @Test
  void getRestaurantProductsByNameAndByCategory_accept() {
    //GIVEN
    UUID restaurantId = Builder.RESTAURANT_ID;
    String name = Builder.PRODUCT_NAME;
    String category = Builder.PRODUCT_CATEGORY;
    when(restaurantRepo.findById(restaurantId)).thenReturn(Optional.of(restaurantEntity));

    //WHEN

    ProductEntity prod1 = Builder.productBuilder();
    ServiceInvocationOutcome<List<ProductEntity>> serviceInvocationOutcome =
        ServiceInvocationOutcome.OK(Collections.singletonList(prod1));
    AggregationResults<ProductEntity> resultMock = mock(AggregationResults.class);
    when(resultMock.getMappedResults()).thenReturn(Collections.singletonList(prod1));
    when(mongoTemplate.aggregate(any(), eq(RestaurantEntity.class), eq(ProductEntity.class)))
        .thenReturn(resultMock);
    ServiceInvocationOutcome<List<ProductEntity>> result =
        productService.getRestaurantProductsByNameAndByCategory(restaurantId, name, category);

    assertEquals(serviceInvocationOutcome.response.get(0), result.response.get(0));

  }

  @Test
  void getRestaurantProductsByNameAndByCategory_reject() {
    //GIVEN
    UUID restaurantId = Builder.RESTAURANT_ID;
    when(restaurantRepo.findById(restaurantId)).thenReturn(Optional.empty());
    ServiceInvocationOutcome<List<ProductEntity>> result =
        productService.getRestaurantProductsByNameAndByCategory(restaurantId,
            Builder.PRODUCT_NAME,
            Builder.PRODUCT_CATEGORY);
    assertTrue(result.notFound);

  }

  @Test
  void getRestaurantProductsByNameAndCategory_noParameters_accept() {
    UUID restaurantId = Builder.RESTAURANT_ID;
    when(restaurantRepo.findById(restaurantId)).thenReturn(Optional.of(restaurantEntity));

    //WHEN
    AggregationResults<ProductEntity> resultMock = mock(AggregationResults.class);
    when(resultMock.getMappedResults())
        .thenReturn(Collections.singletonList(Builder.productBuilder()));
    when(mongoTemplate.aggregate(any(), eq(RestaurantEntity.class), eq(ProductEntity.class)))
        .thenReturn(resultMock);
    ServiceInvocationOutcome<List<ProductEntity>> result =
        productService.getRestaurantProductsByNameAndByCategory(restaurantId, null, null);

    //THEN
    assertTrue(result.ok);

  }

  @Test
  void getProductDetailsFromId_accept() {

    OrderedProduct orderedProduct = Builder.orderedProductBuilder();
    ProductEntity productEntity = Builder.productBuilder();
  //when
    AggregationResults<ProductEntity> resultMock = mock(AggregationResults.class);
    when(resultMock.getUniqueMappedResult()).thenReturn(productEntity);
    when(mongoTemplate.aggregate(any(), eq(RestaurantEntity.class), eq(ProductEntity.class)))
        .thenReturn(resultMock);
    List<DetailedOrderedProductEntity> result =
        productService.getProductsFromIdList(Builder.RESTAURANT_ID,
            Collections.singletonList(orderedProduct));
  //THEN
    assertEquals(1, result.size());
  }

  @Test
  void getProductDetailsFromId_reject() {

    //GIVEN
    OrderedProduct orderedProduct = Builder.orderedProductBuilder();
    //when
    AggregationResults resultMock = mock(AggregationResults.class);
    when(resultMock.getUniqueMappedResult()).thenReturn(null);
    when(mongoTemplate.aggregate(any(), eq(RestaurantEntity.class), eq(ProductEntity.class)))
        .thenReturn(resultMock);
    List<DetailedOrderedProductEntity> result =
        productService.getProductsFromIdList(Builder.RESTAURANT_ID,
            Collections.singletonList(orderedProduct));
    //THEN
    assertEquals(0, result.size());
  }

}