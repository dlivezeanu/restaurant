package com.endava.levelup2020.restaurant.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.model.NewOrder;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.model.Product;
import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import com.endava.levelup2020.restaurant.payload.response.ServiceInvocationOutcome;
import com.endava.levelup2020.restaurant.service.OrderServiceImpl;
import com.endava.levelup2020.restaurant.service.ProductServiceImpl;
import com.endava.levelup2020.restaurant.service.RestaurantServiceImpl;
import com.endava.levelup2020.restaurant.utils.Builder;
import com.endava.levelup2020.restaurant.utils.Convertors;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@ExtendWith(MockitoExtension.class)
class RestaurantControllerTest {

  private static MockMvc mockMvc;

  @InjectMocks
  private RestaurantController restaurantController;
  @Mock
  private RestaurantServiceImpl restaurantService;
  @Mock
  private ProductServiceImpl productService;
  @Mock
  private OrderServiceImpl orderService;
  @Mock
  private Mapper mapper;

  @BeforeEach
  public void initialize() {
    restaurantController = new RestaurantController(restaurantService,
        productService,
        orderService, mapper);
    mockMvc = MockMvcBuilders.standaloneSetup(restaurantController).build();
  }

  @Test
  void getProductsFromRestaurant_noParams_accept() throws Exception {
    //given
    UUID id = Builder.RESTAURANT_ID;

    ArrayList<ProductEntity> products = new ArrayList<>();
    products.add(Builder.productBuilder());
    //when
    ServiceInvocationOutcome<List<Product>> outcome = ServiceInvocationOutcome.OK(products);
    when(productService.getRestaurantProductsByNameAndByCategory(id, null, null))
        .thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(get("/restaurant/" + id + "/product")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());

  }

  @Test
  void getProductsFromRestaurant_withParams_accept() throws Exception {
    //given
    UUID id = Builder.RESTAURANT_ID;
    String name = Builder.PRODUCT_NAME;
    String category = Builder.PRODUCT_CATEGORY;
    ArrayList<ProductEntity> products = new ArrayList<>();
    products.add(Builder.productBuilder());
    //when
    ServiceInvocationOutcome<List<Product>> outcome = ServiceInvocationOutcome.OK(products);
    when(productService.getRestaurantProductsByNameAndByCategory(id, name, category))
        .thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(get("/restaurant/" +id + "/product")
            .param("name", name)
            .param("category", category)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());
  }

  @Test
  void getProductsFromRestaurant_wrongRest_reject() throws Exception {
    //given
    UUID id = Builder.RESTAURANT_ID;
    String name = Builder.PRODUCT_NAME;
    String category = Builder.PRODUCT_CATEGORY;
    //when
    ServiceInvocationOutcome<List<Product>> outcome =
        ServiceInvocationOutcome.NOT_FOUND("NOT FOUND");
    when(productService.getRestaurantProductsByNameAndByCategory(id, name, category))
        .thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(get("/restaurant/" + id + "/product")
            .param("name", name)
            .param("category", category)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(404, response.getStatus());
  }

  @Test
  void getRestaurants() throws Exception {

    when(restaurantService.findByNameAndArea(null, null))
        .thenReturn(Collections.singletonList(Builder.restaurantBuilder()));
    MvcResult result =
        mockMvc.perform(get("/restaurant/")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());
  }

  @Test
  void orderReady_accept() throws Exception {
    //given
    UUID orderId = Builder.ORDER_ID;
    //when
    ServiceInvocationOutcome outcome = ServiceInvocationOutcome.OK(Builder.orderBuilder());
    when(orderService.orderReady(orderId)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(put("/restaurant/{id}/order/ready", orderId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(200, response.getStatus());
  }

  @Test
  void orderReady_notFound_reject() throws Exception {
    //given
    UUID orderId = Builder.ORDER_ID;
    //when
    ServiceInvocationOutcome outcome = ServiceInvocationOutcome.NOT_FOUND("Order not found");
    when(orderService.orderReady(orderId)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(put("/restaurant/{id}/order/ready", orderId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(404, response.getStatus());
  }

  @Test
  void orderReady_badRequest_reject() throws Exception {
    //given
    UUID orderId = Builder.ORDER_ID;
    //when
    ServiceInvocationOutcome outcome =
        ServiceInvocationOutcome.BAD_REQUEST("Order is already ready");
    when(orderService.orderReady(orderId)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(put("/restaurant/{id}/order/ready", orderId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(400, response.getStatus());
  }

  @Test
  void openOrder_accept() throws Exception {
    //given
    UUID restId = Builder.RESTAURANT_ID;
    NewOrder body = Builder.orderPayloadBuilder();
    //when
    Mapper mapper1 = DozerBeanMapperBuilder.create().build();
    ServiceInvocationOutcome<OrderDetails> outcome =
        ServiceInvocationOutcome.OK(mapper1.map(Builder.orderDetailsBuilder(),OrderDetails.class));
    when(orderService.openOrder(restId, body)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(MockMvcRequestBuilders.post("/restaurant/" + restId + "/order/open")
            .content(Convertors.convertObjectToJson(body))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(200, response.getStatus());
  }

  @Test
  void openOrder_restaurantNotFound_reject() throws Exception {
    //given
    UUID restId = Builder.RESTAURANT_ID;
    NewOrder body = Builder.orderPayloadBuilder();
    //when
    ServiceInvocationOutcome<OrderDetails> outcome =
        ServiceInvocationOutcome.NOT_FOUND("Restaurant not found");
    when(orderService.openOrder(restId, body)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(MockMvcRequestBuilders.post("/restaurant/" + restId + "/order/open")
            .content(Convertors.convertObjectToJson(body))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(404, response.getStatus());
  }

  @Test
  void openOrder_badRequest_reject() throws Exception {
    //given
    UUID restId = Builder.RESTAURANT_ID;
    NewOrder body = Builder.orderPayloadBuilder();
    //when
    ServiceInvocationOutcome<OrderDetails> outcome =
        ServiceInvocationOutcome.BAD_REQUEST("The product does not exist");
    when(orderService.openOrder(restId, body)).thenReturn(outcome);
    MvcResult result =
        mockMvc.perform(MockMvcRequestBuilders.post("/restaurant/" + restId + "/order/open")
            .content(Convertors.convertObjectToJson(body))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(400, response.getStatus());
  }

  @Test
  void getOrderDetails_accept() throws Exception {
    //given
    UUID orderId = Builder.ORDER_ID;
    //when
    OrderEntity order = Builder.orderBuilder();
    OrderDetail orderDetail = Builder.orderDetailsBuilder();

    when(orderService.getOrderById(orderId)).thenReturn(Optional.of(order));
    when(orderService.getOrderDetails(order)).thenReturn(Optional.of(orderDetail));

    MvcResult result =
        mockMvc.perform(get("/restaurant/order/" + orderId + "/details")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(200, response.getStatus());

  }

  @Test
  void getOrderDetails_decline() throws Exception {
    //given
    UUID orderId = Builder.ORDER_ID;
    //when
    when(orderService.getOrderById(orderId)).thenReturn(Optional.empty());
    MvcResult result =
        mockMvc.perform(get("/restaurant/order/" + orderId + "/details")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andReturn();
    MockHttpServletResponse response = result.getResponse();
    //then
    assertEquals(404, response.getStatus());

  }



}