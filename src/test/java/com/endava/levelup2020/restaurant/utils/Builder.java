package com.endava.levelup2020.restaurant.utils;

import com.endava.levelup2020.restaurant.entity.CustomerEntity;
import com.endava.levelup2020.restaurant.entity.DetailedOrderedProductEntity;
import com.endava.levelup2020.restaurant.entity.OrderEntity;
import com.endava.levelup2020.restaurant.entity.ProductEntity;
import com.endava.levelup2020.restaurant.entity.RestaurantEntity;
import com.endava.levelup2020.restaurant.model.Customer;
import com.endava.levelup2020.restaurant.model.NewOrder;
import com.endava.levelup2020.restaurant.model.OrderDetails;
import com.endava.levelup2020.restaurant.model.OrderedProduct;
import com.endava.levelup2020.restaurant.payload.response.OrderDetail;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

public class Builder {
  public static final UUID ORDER_ID = UUID.fromString("41ad1e84-fa55-4764-803d-227b5f4e7bf5");
  public static final UUID PRODUCT_ID = UUID.fromString("41ad1e84-fa55-4764-803d-227b5f4e7bf1");
  public static final UUID RESTAURANT_ID = UUID.fromString("41ad1e84-fa55-4764-803d-227b5f4e7bf3");
  public static final String PRODUCT_NAME = "Carbonara", PRODUCT_CATEGORY = "Pizza";

  public static OrderEntity orderBuilder() {
    OrderEntity order = new OrderEntity();
    order.setId(ORDER_ID);
    order.setRestaurantId(RESTAURANT_ID);
    order.setCustomer(customerBuilder());
    order.setStatus(OrderDetails.StatusEnum.CREATED);
    order.setRegistrationDate(new Date());
    order.setProducts(Arrays.asList(orderedProductBuilder()));
    order.setComments("nimic");
    order.setPrice(31.3);
    return order;

  }
  public static DetailedOrderedProductEntity detailedProductBuilder()
  {
    DetailedOrderedProductEntity detailedProduct = new DetailedOrderedProductEntity();
    detailedProduct.setCategory(PRODUCT_CATEGORY);

    detailedProduct.setQuantity(3);
    detailedProduct.setPreparingTime(3);
    detailedProduct.setPrice(12.7);
    detailedProduct.setName(PRODUCT_NAME);
    return detailedProduct;

  }
  public static ProductEntity productBuilder()
  {
    ProductEntity prod1 = new ProductEntity();
    prod1.setId(PRODUCT_ID);
    prod1.setCategory(PRODUCT_CATEGORY);
    prod1.setName(PRODUCT_NAME);
    prod1.setPrice(12.7);
    prod1.setPreparingTime(3);
    prod1.setDescription("");
    return prod1;
  }
  public static OrderedProduct orderedProductBuilder()
  {
    OrderedProduct orderedProduct = new OrderedProduct();
    orderedProduct.setProductId(ORDER_ID);
    orderedProduct.setNoOfItems(3);
    return orderedProduct;
  }
  public static NewOrder orderPayloadBuilder()
  {
    NewOrder newOrder=new NewOrder();
    CustomerEntity customerEntity=customerBuilder();
    Customer customer=new Customer();
    customer.setId(customerEntity.getId());
    customer.setName(customerEntity.getName());
    customer.setAddress(customerEntity.getAddress());
    customer.setPhoneNumber(customerEntity.getPhoneNumber());
    newOrder.setCustomer(customer);
    newOrder.setProducts(Arrays.asList(orderedProductBuilder()));
    newOrder.setComments("Nu avem");
    return newOrder;

  }
  public static RestaurantEntity restaurantBuilder()
  {
    RestaurantEntity restaurantEntity=new RestaurantEntity();
    restaurantEntity.setId(RESTAURANT_ID);
    restaurantEntity.setName("Rest1");
    restaurantEntity.setAddress("BLD. Republicii 33");
    restaurantEntity.setArea("Central");

    ProductEntity prod2=new ProductEntity();
    prod2.setId(UUID.randomUUID());
    prod2.setCategory("Random category");
    prod2.setName("Random prod");
    prod2.setDescription("fara descriere");

    restaurantEntity.setProducts(
        Arrays.asList(
            productBuilder(), prod2
        )
    );
    return restaurantEntity;
  }
  public static CustomerEntity customerBuilder()
  {
    CustomerEntity customerEntity = new CustomerEntity();
    customerEntity.setId(PRODUCT_ID);
    customerEntity.setName("Popescu Ion");
    customerEntity.setAddress("BLD Liberatii 16");
    customerEntity.setPhoneNumber("077336747");
    return customerEntity;
  }
  public static OrderDetail orderDetailsBuilder()
  {
    OrderEntity orderEntity=orderBuilder();
    OrderDetail orderDetails=new OrderDetail();
    orderDetails.setId(orderEntity.getId());
    orderDetails.setComments(orderEntity.getComments());
    orderDetails.setCustomer(orderEntity.getCustomer());
    orderDetails.setProducts(Arrays.asList(detailedProductBuilder()));
    orderDetails.setPrice(orderEntity.getPrice());
    return orderDetails;

  }
}
